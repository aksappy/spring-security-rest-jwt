package be.me.web.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Slf4j
public class JwtTokenSecurityContextConfig implements SecurityContextRepository {

    @Override
    @SneakyThrows
    public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
        log.debug(">> loadContext- {}", requestResponseHolder.getRequest().getRequestURI());
        HttpServletRequest request = requestResponseHolder.getRequest();
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        String authentication = request.getHeader("Authorization");
        if (request.getRequestURI().startsWith("/pub")) {
            return securityContext;
        }

        if (null == authentication || authentication.isEmpty()) {
            return securityContext;
        }
        log.debug(">> authentication header - {}", authentication);
        try {
            UserAuthentication userAuthentication = decodeToken(getTokenFromBearer(request.getHeader("Authorization")));
            log.debug("{} - {}", userAuthentication.getUsername(), userAuthentication.getAuthorities());
            securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(userAuthentication, "", userAuthentication.getAuthorities()));
            return securityContext;
        } catch (Exception e) {
            log.error(null, e);
        }
        return securityContext;
    }

    protected String getTokenFromBearer(String headerValue) {
        if (headerValue == null) return "";
        return headerValue.replace("Bearer ", "");
    }

    protected UserAuthentication decodeToken(String token) {
        DecodedJWT jwt = JWT.decode(token);
        String subject = jwt.getId();
        log.debug("subject = {}", subject);
        Map<String, Claim> claims = jwt.getClaims();
        List<GrantedAuthority> roles = Arrays.stream(claims.get("roles").asString().split(","))
                .map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        UserAuthentication user = UserAuthentication.builder()
                .username(jwt.getSubject())
                .authorities(roles)
                .build();
        return user;
    }

    @Override
    public void saveContext(SecurityContext context, HttpServletRequest request, HttpServletResponse response) {
        log.debug("<<<<<RestSecurityContextRepository.saveContext>>>>> {}", request.getRequestURI());

        // Response header could already have the token if this is the login processing URL
//        if (response.getHeader(KEY_HEADER_TOKEN) == null) {
//            response.addHeader(KEY_HEADER_TOKEN, createTokenFrom(context.getAuthentication()));
//        }

    }

    private static final String KEY_HEADER_TOKEN = "X-AUTH";

    @Override
    public boolean containsContext(HttpServletRequest request) {
        // TODO Auto-generated method stub
        return false;
    }


    public String createTokenFrom(Authentication authentication) {
        log.debug("<<<<<" + "TokenManagerJwt.createTokenFrom(authentication)" + ">>>>>");
        if (authentication == null) return null;

        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        String userName = userDetails.getUsername();
        String authorities = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining(","));

        return JWT.create()
                .withClaim("username", userName)
                .withClaim("roles", authorities)
                .sign(Algorithm.HMAC256("secret"));
    }

}
