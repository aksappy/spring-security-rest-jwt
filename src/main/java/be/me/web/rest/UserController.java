package be.me.web.rest;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("sec")
public class UserController {

    @GetMapping("me")
    @RolesAllowed("ROLE_ADMIN")
    public String getMe(Authentication authentication) {
        log.debug("{}", authentication.getAuthorities());
        return authentication.getName();
    }

}
