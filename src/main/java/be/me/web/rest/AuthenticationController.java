package be.me.web.rest;

import be.me.dto.Login;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("pub")
public class AuthenticationController {

    @PostMapping("auth")
    public Login.Response auth(@RequestBody Login.Request loginRequest) {
        var token = JWT.create()
                .withSubject(loginRequest.getUsername())
                .withClaim("roles", "ROLE_USER")
                .sign(Algorithm.HMAC256("secret"));
        return new Login.Response(token);
    }
}
