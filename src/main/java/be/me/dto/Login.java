package be.me.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

public class Login {

    @Data
    public static class Request {
        private String username;
        private String password;
    }

    @Data
    @AllArgsConstructor
    public static class Response {
        private String accessToken;
    }
}
